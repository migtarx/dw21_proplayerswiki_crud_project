<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/PlayerDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Player.php');


function indexAction() {
    $creatureDAO = new PlayerDAO();
    return $creatureDAO->selectAll();
}

?>