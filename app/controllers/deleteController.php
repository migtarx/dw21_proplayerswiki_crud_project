<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/PlayerDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Player.php');

$creatureDAO = new PlayerDAO();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
//Llamo que hace la edición contra BD
    deleteAction();
}

function deleteAction() {
    $id = $_GET["id"];

    $creatureDAO = new PlayerDAO();
    $creatureDAO->delete($id);

    header('Location: ../../index.php');
}
?>

