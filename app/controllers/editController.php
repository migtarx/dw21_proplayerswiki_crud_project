<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/PlayerDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Player.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    $player = new Player();
    $player->setPlayerId((int)$_POST["id"]);
    $player->setNickname($_POST["nickname"]);
    $player->setAge($_POST["age"]);
    $player->setBio($_POST["bio"]);
    $player->setNationality($_POST["nationality"]);
    $player->setTeam($_POST["team"]);
    $player->setRole($_POST["role"]);
    $player->setAvatar($_POST["avatar"]);

    $playerDAO = new PlayerDAO();
    $playerDAO->update($player);

    header('Location: ../../index.php');
}

?>

