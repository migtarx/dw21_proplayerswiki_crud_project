<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/PlayerDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Player.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $nickname = ValidationsRules::test_input($_POST["nickname"]);
    $bio = ValidationsRules::test_input($_POST["bio"]);
    $nationality = ValidationsRules::test_input($_POST["nationality"]);
    $team = ValidationsRules::test_input($_POST["team"]);
    $role = ValidationsRules::test_input($_POST["role"]);
    $age = ValidationsRules::test_input($_POST["age"]);
    $avatar = ValidationsRules::test_input($_POST["avatar"]);
    // TODOD hacer uso de los valores validados 
    $player = new Player();
    $player->setNickname($_POST["nickname"]);
    $player->setBio($_POST["bio"]);
    $player->setAvatar($_POST["avatar"]);
    $player->setNationality($_POST["nationality"]);
    $player->setTeam($_POST["team"]);
    $player->setRole($_POST["role"]);
    $player->setAge($_POST["age"]);

    //Creamos un objeto PlayerDAO para hacer las llamadas a la BD
    $playerDAO = new PlayerDAO();
    $playerDAO->insert($player);
    
    header('Location: ../../index.php');
    
}
?>

