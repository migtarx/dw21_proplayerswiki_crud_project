<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Gestión de Criaturas</title>
        <!-- Bootstrap Core CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-light navbar-fixed-top navbar-expand-md bg-faded" role="navigation" style="background-color: burlywood !important">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="../../index.php"> <img src="../../assets/img/small-logo.png" alt="" ></a>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                  <ul class="navbar-nav mr-auto ">
                    <li class="nav-item active">
                      <a type="button" class="btn btn-info " href="#">Insert new player</a>
                    </li>
                  </ul>
                </div>
              </nav>
        <!-- Page Content -->
         <div class="container">
            <form class="form-horizontal" method="post" action="../controllers/insertController.php">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nickname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nickname" id="title" placeholder="Player Nickname" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="age" class="col-sm-2 control-label">Age</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="age" name="age" placeholder="Player Age" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bio" class="col-sm-2 control-label">Biography</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="bio" name="bio" placeholder="Player Age" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nationality" class="col-sm-2 control-label">Nationality</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="nationality" name="nationality" placeholder="Player Nationality" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nationality" class="col-sm-2 control-label">Team</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="team" name="team" placeholder="Player team" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="role" class="col-sm-2 control-label">Role</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" id="role" name="role" placeholder="Player Role" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="avatar" class="col-sm-2 control-label">Avatar</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="cover" name="avatar" placeholder="Avatar" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Insert New Player</button>
                    </div>
                </div>
            </form>
            <!-- Footer -->
           <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; migtarx 2021</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /.container -->
        <!-- jQuery -->
        <script src="../../assets/js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../../assets/js/bootstrap.min.js"></script>
    </body>
</html>


