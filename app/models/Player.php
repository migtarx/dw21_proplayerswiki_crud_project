<?php

class Player {

    private $playerId;
    private $nickname;
    private $age;
    private $nationality;
    private $team;
    private $bio;
    private $role;
    private $avatar;

    public function __construct() {
        
    }

    public function getPlayerId() {
        return $this->playerId;
    }

    public function getBio() {
        return $this->bio;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getNationality()
    {
        return $this->nationality;
    }

    public function getTeam()
    {
        return $this->team;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public  function getAvatar() {
        return $this->avatar;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
    }

    public function setPlayerId($playerId) {
        $this->playerId = $playerId;
    }

    function setBio($bio) {
        $this->bio = $bio;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    public function setTeam($team)
    {
        $this->team = $team;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }



//Función para pintar cada criatura
    function creature2HTML() {
        $result = '<div class=" col-md-4 ">';
         $result .= '<div class="card ">';
          $result .= ' <img class="card-img-top rounded mx-auto d-block avatar" src='.$this->getAvatar().' alt="Card image cap">';
            $result .= '<div class="card-block">';
                $result .= '<h2 class="card-title">' . $this->getNickname() . '</h2>';
                $result .= '<p class=" card-text description">'.$this->getBio().'</p>';
                $result .= '<h6>Age: '.$this->getAge().'</h6>';
                $result .= '<h6>Nationality: '.$this->getNationality().'</h6>';
                $result .= '<h6>Team: '.$this->getTeam().'</h6>';
                $result .= '<h6>Role: '.$this->getRole().'</h6>';
             $result .= '</div>';
             $result .= ' <div  class=" btn-group card-footer" role="group">';
                $result .= '<a type="button" class="btn btn-secondary" href="app/views/detail.php?id='.$this->getPlayerId().'">Detalles</a>';
                $result .= '<a type="button" class="btn btn-success" href="app/views/edit.php?id='.$this->getPlayerId().'">Modificar</a> ';
                $result .= '<a type="button" class="btn btn-danger" href="app/controllers/deleteController.php?id='.$this->getPlayerId().'">Borrar</a> ';
            $result .= ' </div>';
         $result .= '</div>';
     $result .= '</div>';
        
        
        return $result;
    }
    
    
}
