CREATE DATABASE IF NOT EXISTS proplayerswikidb;

USE proplayerswikidb;

CREATE TABLE IF NOT EXISTS players (
	playerId int not null AUTO_INCREMENT,
	nickname varchar(255),
	age int,
	bio text,
	nationality varchar(255),
	team varchar(255),
	role varchar(255),
	avatar varchar(255),
	PRIMARY KEY( playerId )
);

INSERT INTO `players` VALUES (1, 'Alejandro "Alex" Masanet', 25, 'Alejandro "ALEX" Masanet (born December 15, 1995) is a Spanish professional Counter-Strike: Global Offensive player.', 'Spanish', 'Movistar Riders', 'AWP, IGL' , 'assets/img/alex_csgo.jpg' );
INSERT INTO `players` VALUES (1, 'Alejandro "Mopoz" Fernández', 25, 'Alejandro "mopoz" Fernández-Quejo Cano (born August 7, 1996) is a Spanish professional Counter-Strike: Global Offensive player for Movistar Riders.', 'Spanish', 'Movistar Riders', 'Entry Fragger' , 'assets/img/mopoz_csgo.jpg' );
