<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class PlayerDAO {

    //Se define una constante con el nombre de la tabla
    const PLAYERS_TABLE = 'players';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . PlayerDAO::PLAYERS_TABLE;
        $result = mysqli_query($this->conn, $query);
        $players = array();

        while ($playerBD = mysqli_fetch_array($result)) {
            $player = new Player();
            $player->setPlayerId($playerBD["playerId"]);
            $player->setNickname($playerBD["nickname"]);
            $player->setBio($playerBD["bio"]);
            $player->setAvatar($playerBD["avatar"]);
            $player->setNationality($playerBD["nationality"]);
            $player->setTeam($playerBD["team"]);
            $player->setRole($playerBD["role"]);
            $player->setAge($playerBD["age"]);
            
            array_push($players, $player);
        }
        return $players;
    }

    public function insert($player) {
        $query = "INSERT INTO " . PlayerDAO::PLAYERS_TABLE .
                " (nickname, age, bio, nationality, team, role, avatar) VALUES(?,?,?,?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $nickname = $player->getNickname();
        $bio = $player->getBio();
        $age = $player->getAge();
        $age = (int) $age;
        $nationality = $player->getNationality();
        $team = $player->getTeam();
        $role = $player->getRole();
        $avatar = $player->getAvatar();
        
        mysqli_stmt_bind_param($stmt, 'sssssss',  $nickname, $age, $bio, $nationality, $team, $role, $avatar);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT nickname, age, bio, nationality, team, role, avatar FROM " . PlayerDAO::PLAYERS_TABLE . " WHERE playerId=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $nickname, $age, $bio, $nationality, $team, $role, $avatar);

        $player = new Player();
        while (mysqli_stmt_fetch($stmt)) {
            $player->setPlayerId($id);
            $player->setNickname($nickname);
            $player->setAge($age);
            $player->setBio($bio);
            $player->setNationality($nationality);
            $player->setTeam($team);
            $player->setRole($role);
            $player->setAvatar($avatar);
       }
        return $player;
    }

    public function update($player) {
        $query = "UPDATE " . PlayerDAO::PLAYERS_TABLE .
                " SET nickname=?, age=?, bio=?, nationality=?, team=?, role=?, avatar=? "
                . " WHERE playerId=? ";
        $stmt = mysqli_prepare($this->conn, $query);
        $playerId = $player->getPlayerId();
        $nickname = $player->getNickname();
        $age = $player->getAge();
        $age = (int) $age;
        $bio = $player->getBio();
        $nationality = $player->getNationality();
        $team = $player->getTeam();
        $role = $player->getRole();
        $avatar = $player->getAvatar();
        mysqli_stmt_bind_param($stmt, 'sisssssi',  $nickname, $age, $bio, $nationality, $team, $role, $avatar, $playerId);
        return $stmt->execute();
    }

    public function delete($id) {
        $query = "DELETE FROM " . PlayerDAO::PLAYERS_TABLE . " WHERE playerId=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
